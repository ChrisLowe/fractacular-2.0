package com.lowware.fractal.formula;


import com.lowware.fractal.FractalJPanel;



public class Mandelbrot extends Fractal
{
	
	public int performCalculation(double xco, double yco) {
		double z = 0;
		double c = 0;
		int iterations = 0;
		
        while ((z <= 2) && (c <= 2) && (iterations < getMaxIterations())) {
            double temp = (z * z) - (c * c) + xco;
            c = (2 * z * c) + yco;
            z = temp;
            iterations++;
        }
		
		return iterations;
	}
	
 
    
    public String getName() {
    	return "Mandelbrot";
    }
    
    public String getFractalDetails() {
        StringBuffer b = new StringBuffer();
        b.append("Mandelbrot ");
        b.append("Z(n+1) = Z(n)^2 + c, over " + getMaxIterations() + " iterations at");
        b.append(" ");
        b.append("(" + getCurrentMinX() + ", " + getCurrentMinY() + ")");
        b.append(" .. ");
        b.append("(" + getCurrentMaxX() + ", " + getCurrentMinY() + ")");
        return b.toString();
    }
    
    public Mandelbrot(FractalJPanel panel) {
        super(panel);
    }
}
