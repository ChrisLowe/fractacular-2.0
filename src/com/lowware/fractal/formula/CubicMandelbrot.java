package com.lowware.fractal.formula; 

import com.lowware.fractal.FractalJPanel;


/**
 * A cubic mandelbrot given by the forumula
 *
 *   Z(n+1) = Z^3 + c
 *
 * 
 */
public class CubicMandelbrot extends Fractal
{
	
	public int performCalculation(double xco, double yco) {
		double z = 0;
		double c = 0;
		int iterations = 0;
		
        while ((z <= 2) && (c <= 2) && (iterations < getMaxIterations())) {                
            double zlast = (z*z*z - 3*z*c*c) + xco;
            c = (3*z*z*c - c*c*c) + yco;
            z = zlast;                               
            iterations++;
        }
        
        return iterations;
	}

    
    public String getFractalDetails() {
        StringBuffer b = new StringBuffer();
        b.append("Cubic Mandelbrot ");
        b.append("Z(n+1) = Z(n)^3 + c");
        b.append("   ");
        b.append("(" + getCurrentMinX() + ", " + getCurrentMinY() + ")");
        b.append(" .. ");
        b.append("(" + getCurrentMaxX() + ", " + getCurrentMinY() + ")");
        return b.toString();
    }
    


	@Override
	public String getName() {
		return "Cubic Mandelbrot";
	}

    public CubicMandelbrot(FractalJPanel panel) {
        super(panel);
    }

	


}
