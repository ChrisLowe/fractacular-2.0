package com.lowware.fractal.formula;


import com.lowware.fractal.FractalJPanel;



public class QuadraticMandelbrot extends Fractal
{
	
	public int performCalculation(double xco, double yco) {
		double z = 0;
		double c = 0;
		int iterations = 0;


		
        while ((z <= 2) && (c <= 2) && (iterations < getMaxIterations())) {
        	
        	
        	double temp = (z*z*z*z) - (6*z*z*c*c) + (c*c*c*c) + xco;
            c =  (4*z*z*z*c) - (4*z*c*c*c) + yco;
            z = temp;
            iterations++;
        	
 
             
        	
        }
		
		return iterations;
	}
	
 
    
    public String getName() {
    	return "Quadratic Mandelbrot";
    }
    
    public String getFractalDetails() {
        StringBuffer b = new StringBuffer();
        b.append("Mandelbrot ");
        b.append("Z(n+1) = Z(n)^4 + c, over " + getMaxIterations() + " iterations at");
        b.append(" ");
        b.append("(" + getCurrentMinX() + ", " + getCurrentMinY() + ")");
        b.append(" .. ");
        b.append("(" + getCurrentMaxX() + ", " + getCurrentMinY() + ")");
        return b.toString();
    }
    
    public QuadraticMandelbrot(FractalJPanel panel) {
        super(panel);
    }
}

