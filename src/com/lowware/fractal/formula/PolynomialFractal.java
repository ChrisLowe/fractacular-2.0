package com.lowware.fractal.formula;


import com.lowware.fractal.FractalJPanel;



public class PolynomialFractal extends Fractal
{
	
	public int performCalculation(double xco, double yco) {
		double z = 0;
		double c = 0;
		int iterations = 0;
        double zlast = 0;

		
        while ((z <= 2) && (c <= 2) && (iterations < getMaxIterations())) {
            double temp = (z * z + zlast) - (c * c) + xco;
            c = (2 * z * c) + yco;
            z = temp;
            zlast = z;
            iterations++;
        }
		
		return iterations;
	}
	
 
    
    public String getName() {
    	return "Polynomial Fractal";
    }
    
    public String getFractalDetails() {
        StringBuffer b = new StringBuffer();
        b.append("Polynomial ");
        b.append("Z(n+1) = Z(n)^2 + c with Z(n-1) retention, over " + getMaxIterations() + " iterations at");
        b.append(" ");
        b.append("(" + getCurrentMinX() + ", " + getCurrentMinY() + ")");
        b.append(" .. ");
        b.append("(" + getCurrentMaxX() + ", " + getCurrentMinY() + ")");
        return b.toString();
    }
    
    public PolynomialFractal(FractalJPanel panel) {
        super(panel);
    }
}
