

package com.lowware.fractal.formula;


import com.lowware.fractal.FractalJPanel;



public class SextupicMandelbrot extends Fractal
{
	
	public int performCalculation(double xco, double yco) {
		double z = 0;
		double c = 0;
		int iterations = 0;


		
        while ((z <= 16) && (c <= 16) && (iterations < getMaxIterations())) {
        	
        	
        	//double temp = ((z*z*z*z*z) - (10*z*z*z*c*c) - (5*z*c*c*c*c)) + xco;
            //c = ( (5*z*z*z*z*c) - (10*z*z*c*c*c)  + (c*c*c*c*c)) + yco;
            
        	//double temp = (z*z*z*z*z) - (7*z*z*z*c*c) + (2*z*z*z*c) + (z*c*c*c*c) + (2*z*c*c*c) + xco;
        	//c = (3*z*z*z*z*c) - (7*z*z*c*c*c) + (2*z*c*c*c*c) + (c*c*c*c*c) + yco;
        	
        	//double temp = (z*z*z*z*z) + (5*z*c*c*c*c) - (10*z*z*z*c*c) + xco;
        	//c = (c*c*c*c*c) + (5*z*z*z*z*c) - (10*z*z*c*c*c) +  yco;
        	
        	double temp = (z*z*z*z*z*z) - (c*c*c*c*c*c) - (15*z*z*z*z*c*c) + (15*z*z*c*c*c*c) + xco;
        	c = (6*z*c*c*c*c*c) + (6*z*z*z*z*z*c) - (20*z*z*z*c*c*c) + yco;
        	
        	z = temp;
            iterations++;
        	
 
             
        	
        }
		
		return iterations;
	}
	
 
    
    public String getName() {
    	return "Sextupic Mandelbrot";
    }
    
    public String getFractalDetails() {
        StringBuffer b = new StringBuffer();
        b.append("Mandelbrot ");
        b.append("Z(n+1) = Z(n)^6 + c, over " + getMaxIterations() + " iterations at");
        b.append(" ");
        b.append("(" + getCurrentMinX() + ", " + getCurrentMinY() + ")");
        b.append(" .. ");
        b.append("(" + getCurrentMaxX() + ", " + getCurrentMinY() + ")");
        return b.toString();
    }
    
    public SextupicMandelbrot(FractalJPanel panel) {
        super(panel);
    }
}


