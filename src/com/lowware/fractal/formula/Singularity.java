package com.lowware.fractal.formula;

import com.lowware.fractal.FractalJPanel;

public class Singularity extends Fractal {

	public Singularity(FractalJPanel panel) {
		super(panel);
		
	}

	@Override
	public int performCalculation(double xco, double yco) {
		
		int iterations = 0;
		double z = 0;
		double c = 0;
		
		double invpi = 0.318309886;
        //calculate escape iterations
        while ((z <= 4) && (c <= 4) && (iterations < getMaxIterations())) {
            double temp = (z * z) - (c * c)  + xco;
            double temp2 = (2 * z * c) + yco;
            
            
            c = temp2;
            z = temp * invpi;
            
           
            iterations++;
        }
		
		return iterations;
	}

	@Override
	public String getName() {
		return "Singularity";
	}

	@Override
	public String getFractalDetails() {
		// TODO Auto-generated method stub
		return null;
	}

}
