package com.lowware.fractal.formula;


import com.lowware.fractal.FractalJPanel;



public class SpiderWeb extends Fractal
{
	
	public int performCalculation(double xco, double yco) {
		double z = 0;
		double c = 0;
		int iterations = 0;


		
        while ((z <= 2) && (c <= 2) && (iterations < getMaxIterations())) {
        	
        	double znext = Math.abs(z * z - c * c + xco);
            c =  Math.abs(2 * z * c + yco);
            z = znext;
            iterations++;
             
        	
        }
		
		return iterations;
	}
	
 
    
    public String getName() {
    	return "SpiderWeb";
    }
    
    public String getFractalDetails() {
        StringBuffer b = new StringBuffer();
        b.append("Mandelbrot ");
        b.append("Z(n+1) = Z(n)^2 + c with abs applied on real and imaginary, over " + getMaxIterations() + " iterations at");
        b.append(" ");
        b.append("(" + getCurrentMinX() + ", " + getCurrentMinY() + ")");
        b.append(" .. ");
        b.append("(" + getCurrentMaxX() + ", " + getCurrentMinY() + ")");
        return b.toString();
    }
    
    public SpiderWeb(FractalJPanel panel) {
        super(panel);
    }
}

