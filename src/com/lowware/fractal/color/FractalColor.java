package com.lowware.fractal.color;

public class FractalColor {

	public int r;
	public int g;
	public int b;

	
	public FractalColor(int r, int g, int b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	
}
