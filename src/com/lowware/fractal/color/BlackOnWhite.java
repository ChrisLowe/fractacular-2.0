package com.lowware.fractal.color;

public class BlackOnWhite implements FractalColorAlgorithm {

	@Override
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		int red = Math.abs(255 - iterations);
		int green = Math.abs(255 - iterations);
		int blue = Math.abs(255 - iterations);
		
		return new FractalColor(red, green, blue);
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Black on White";
	}

}
