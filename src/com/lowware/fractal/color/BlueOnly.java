package com.lowware.fractal.color;


public class BlueOnly implements FractalColorAlgorithm {

	@Override
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		int red = 0;
		int green = 0;
		int blue = iterations * 5 % 255;
		
		return new FractalColor(red, green, blue);
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Blue";
	}

}
