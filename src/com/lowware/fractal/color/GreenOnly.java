package com.lowware.fractal.color;


public class GreenOnly implements FractalColorAlgorithm {

	@Override
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		int red = 0;
		int green = iterations * 5 % 255;
		int blue = 0;
		
		return new FractalColor(red, green, blue);
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Green";
	}

}
