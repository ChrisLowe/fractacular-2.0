package com.lowware.fractal.color;

public class Trippy implements FractalColorAlgorithm {

	@Override
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
       
	}

	@Override
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		 double magx = Math.sqrt(xco*xco)+(yco*yco);
         double log2magx = Math.log(Math.log(magx)/Math.log(2));
         double smoothing = iterations + 1 - log2magx;   
         int green = g + (int) smoothing % 255;
         int red = r + 6 * green % 255;
         int blue = b + (green / 2) % 255;
         
         return new FractalColor(red, green, blue);
		
	}

	@Override
	public String getName() {
		return "Trippy";
	}

}
