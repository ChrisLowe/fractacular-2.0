package com.lowware.fractal.color;


public class RedOnly implements FractalColorAlgorithm {

	@Override
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		int red = iterations * 5 % 255;
		int green = 0;
		int blue = 0;
		
		return new FractalColor(red, green, blue);
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Red";
	}

}
