package com.lowware.fractal.color;

public class WhiteOnBlack implements FractalColorAlgorithm {


	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		int red = iterations;
		int green = iterations;
		int blue = iterations;
		
		return new FractalColor(red, green, blue);
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		
		int red = Math.abs(255 - iterations);
		int green = Math.abs(255 - iterations);
		int blue = Math.abs(255 - iterations);
		
		return new FractalColor(red, green, blue);
	}

	public String getName() {
		return "White on Black";
	}

}
