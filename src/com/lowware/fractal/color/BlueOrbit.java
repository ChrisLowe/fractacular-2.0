package com.lowware.fractal.color;

public class BlueOrbit implements FractalColorAlgorithm{

	
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		double magx = Math.sqrt((xco*xco) + (yco*yco));
        double logmagxlog3 = Math.log(Math.log(magx)/Math.log(3));
        double smoothing = iterations + 1 - logmagxlog3;
        int green =(int) smoothing % 255;
        int red = green / 2;
        int blue = iterations + (green * 4) % 255;

        
        return new FractalColor(red, green, blue);
		
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Blue Orbit";
	}

}
