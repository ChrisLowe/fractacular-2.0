package com.lowware.fractal.color;

public class GreyScale implements FractalColorAlgorithm {


	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		
		int red = iterations % 255;
		int green = iterations % 255;
		int blue = iterations % 255;
		
		return new FractalColor(red, green, blue);
		
	}
	
	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Grey Scale";
	}

}
