package com.lowware.fractal.color;

public class PurpleHaze implements FractalColorAlgorithm {
	
	public PurpleHaze() { 
		
	}
	
	public FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
        double magx = Math.sqrt((xco*xco)+(yco*yco));
        double log2magx = Math.log(Math.log(magx)/Math.log(2));
        double smoothing = iterations + 1 - log2magx;
        
        
        int green = g + ((int) smoothing * 2 % 255);
        int red = r + ((int) smoothing * 3 % 255);
        int blue = b + ((int) smoothing * 4 % 255);
        
        return new FractalColor(red, green, blue);
	}


	public FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b) {
		return new FractalColor(r, g, b);
	}
	
	public String getName() {
		return "Purple Haze";
	}

}
