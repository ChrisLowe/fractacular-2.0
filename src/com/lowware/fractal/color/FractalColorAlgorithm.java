package com.lowware.fractal.color;

public interface FractalColorAlgorithm {

	public abstract FractalColor getInteriorColor(int iterations, double xco, double yco, int r, int g, int b);
	public abstract FractalColor getExteriorColor(int iterations, double xco, double yco, int r, int g, int b);
	public abstract String getName();
	
	
}
