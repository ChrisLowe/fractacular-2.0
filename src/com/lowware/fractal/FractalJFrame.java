package com.lowware.fractal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import com.lowware.fractal.formula.*;
import com.lowware.fractal.color.*;

public class FractalJFrame extends JFrame implements MouseListener, MouseMotionListener, KeyListener {


	private static final String[] FRACTALS = { 
		"com.lowware.fractal.formula.Mandelbrot",
		"com.lowware.fractal.formula.CubicMandelbrot",
		"com.lowware.fractal.formula.Singularity",
		"com.lowware.fractal.formula.PolynomialFractal",
		"com.lowware.fractal.formula.QuadraticMandelbrot",
		"com.lowware.fractal.formula.SeptupicMandelbrot",
		"com.lowware.fractal.formula.SextupicMandelbrot",
		"com.lowware.fractal.formula.SpiderWeb"
		
	};
	
	private static final String[] COLORS = {
		"com.lowware.fractal.color.GreenKiss",
		"com.lowware.fractal.color.GreyScale",
		"com.lowware.fractal.color.BlueOrbit",
		"com.lowware.fractal.color.PurpleHaze",
		"com.lowware.fractal.color.WhiteOnBlack",
		"com.lowware.fractal.color.BlackOnWhite",
		"com.lowware.fractal.color.Trippy",
		"com.lowware.fractal.color.RedDusk",
		"com.lowware.fractal.color.RedOnly",
		"com.lowware.fractal.color.BlueOnly",
		"com.lowware.fractal.color.GreenOnly"
		
		
	};
	
	
	private static final long serialVersionUID = 1L;
	
	private int screenWidth = 500;
	private int screenHeight = 500;
	
	
	
	private Point mouseDrag;
	
	private static final String TITLE = "Fractacular 2.0";
	
	private static final double DEFAULT_MINX = -2.5;
    private static final double DEFAULT_MAXX = 1;
    private static final double DEFAULT_MINY = -1.25;
    private static final double DEFAULT_MAXY = 1.25;
    
    /**
     *  Default escape time
     */
    public static final int DEFAULT_MAX_ITERATIONS = 64;
	
    JColorChooser colorChoose;
    JDialog colorDialog;
    
    
    private FractalJPanel panel;
    private Fractal fractal;
	
    public FractalJFrame() {
    	super(TITLE);

    	// Handle window resizing
    	addComponentListener(new ComponentListener() {
			
			public void componentResized(ComponentEvent e) {
			
				int newHeight = e.getComponent().getHeight();
				int newWidth = e.getComponent().getWidth();
				
				panel.resizePanel(newWidth, newHeight);
				fractal.setWidth(newWidth);
				fractal.setHeight(newHeight);
				fractal.generateFractal(fractal.getCurrentMinX(), fractal.getCurrentMaxX(), fractal.getCurrentMinY(), fractal.getCurrentMaxY());
				panel.repaint();
								
			}

	
			public void componentHidden(ComponentEvent e) {}
			public void componentMoved(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
			
		});
    	

    		
 
    
    		
    		
    	
    	
        //Configure the JFrame
    	
    	panel = new FractalJPanel(screenWidth, screenHeight);
        getContentPane().add(panel);
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(this);        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(screenWidth, screenHeight);
        setJMenuBar(createJMenuBar());
       
        //Center the JFrame
        Dimension screensize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int width = screensize.width;
        int height = screensize.height;
        int width_diff = width - screenWidth;
        int height_diff = height - screenHeight;
        setLocation(width_diff / 2, height_diff / 2);
        
        setResizable(true);
        setVisible(true);
        
        fractal = new Mandelbrot(panel);
    	
    }
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		 double minx = fractal.getCurrentMinX();
	        double maxx = fractal.getCurrentMaxX();
	        double miny = fractal.getCurrentMinY();
	        double maxy = fractal.getCurrentMaxY();
	        double panSize = (maxx - minx) / 4;
	        
	        switch (e.getKeyCode()) {
	            case KeyEvent.VK_ESCAPE:  //Return to default
	            	fractal.setMaxIterations(DEFAULT_MAX_ITERATIONS);
	            	fractal.setRed(0);
	            	fractal.setBlue(0);
	                fractal.setGreen(0);
	                fractal.setInternalBlue(0);
	                fractal.setInternalRed(0);
	                fractal.setInternalGreen(0);
	                fractal.generateFractal(DEFAULT_MINX, DEFAULT_MAXX, DEFAULT_MINY, DEFAULT_MAXY);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_LEFT:    //Pan left
	                fractal.generateFractal(minx - panSize, maxx - panSize, miny, maxy);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_RIGHT:   //Pan right
	                fractal.generateFractal(minx + panSize, maxx + panSize, miny, maxy);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_UP:      //Pan up
	                fractal.generateFractal(minx, maxx, miny - panSize, maxy - panSize);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_DOWN:    //Pan down
	                fractal.generateFractal(minx, maxx, miny + panSize, maxy + panSize);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_A:    //Pan left
	                fractal.generateFractal(minx - panSize, maxx - panSize, miny, maxy);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_D:   //Pan right
	                fractal.generateFractal(minx + panSize, maxx + panSize, miny, maxy);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_W:      //Pan up
	                fractal.generateFractal(minx, maxx, miny - panSize, maxy - panSize);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_S:    //Pan down
	                fractal.generateFractal(minx, maxx, miny + panSize, maxy + panSize);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_P:       //Save to file
	                panel.saveToFile(fractal.getFractalDetails());
	                break;
	                
	    
	                
	            case KeyEvent.VK_L:    //Increase Iterations
	                fractal.setMaxIterations(fractal.getMaxIterations() + 50);
	                fractal.generateFractal(minx, maxx, miny, maxy);
	                repaint();
	                break;
	                
	            case KeyEvent.VK_K:    //Decrease Iterations
	                fractal.setMaxIterations(fractal.getMaxIterations() - 50);
	                fractal.generateFractal(minx, maxx, miny, maxy);
	                repaint();
	                break;
	            
	            default:
	                break;
	            }
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
	      
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		if ((mouseDrag.x == e.getPoint().x) && (mouseDrag.y == e.getPoint().y)) {
		
	        switch (e.getModifiers()) {
	        case MouseEvent.BUTTON1_MASK:   //Left mouse button -- zoom in
	            double newMinX = fractal.getScaledXCoordinate(e.getX() - (panel.getWidth() / 4));
	            double newMaxX = fractal.getScaledXCoordinate(e.getX() + (panel.getWidth() / 4));
	            double newMinY = fractal.getScaledYCoordinate(e.getY() - (panel.getHeight() / 4));
	            double newMaxY = fractal.getScaledYCoordinate(e.getY() + (panel.getHeight() / 4));
	            
	          
	            
	            fractal.generateFractal(newMinX, newMaxX, newMinY, newMaxY);
	            panel.repaint();
	            break;
	            
	        case MouseEvent.BUTTON2_MASK:   //Middle mouse button -- does nothing
	            break;    
	        
	        case MouseEvent.BUTTON3_MASK:   //Right mouse button -- zoom out
	            double newMinX3 = fractal.getScaledXCoordinate(e.getX() - panel.getWidth());
	            double newMaxX3 = fractal.getScaledXCoordinate(e.getX() + panel.getWidth());
	            double newMinY3 = fractal.getScaledYCoordinate(e.getY() - panel.getHeight());
	            double newMaxY3 = fractal.getScaledYCoordinate(e.getY() + panel.getHeight());
	            fractal.generateFractal(newMinX3, newMaxX3, newMinY3, newMaxY3);
	            panel.repaint();
	            break;
	            
	        default:
	            break;
	        }
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseDrag = e.getPoint();
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if ((mouseDrag.x != e.getPoint().x) && (mouseDrag.y != e.getPoint().y)) {
	
			Point p = e.getPoint();	
			
	        double newMinX = fractal.getScaledXCoordinate(p.x - panel.getWidth() / 2);
	        double newMaxX = fractal.getScaledXCoordinate(p.x + panel.getWidth() / 2);
	        double newMinY = fractal.getScaledYCoordinate(p.y - panel.getHeight() / 2);
	        double newMaxY = fractal.getScaledYCoordinate(p.y + panel.getHeight() / 2);
	        
	        fractal.generateFractal(newMinX, newMaxX, newMinY, newMaxY);
	        panel.repaint();
			
		}
		
		
	}
	
	private JMenuBar createJMenuBar() {
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		JMenu navMenu = new JMenu("Navigation");
		JMenu formulaMenu = new JMenu("Formula");
		JMenu colorMenu = new JMenu("Color");
		JMenu textMenu = new JMenu("Textbrot");
		

		
		/////////////////////////////////////////////////////////////
		//
		//		File Menu
		//
		/////////////////////////////////////////////////////////////
		JMenuItem screen = new JMenuItem("Screen shot");
		screen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { 
				panel.saveToFile(fractal.getFractalDetails());
			}
		});
		fileMenu.add(screen);
		
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { 
				
			}
		});
		fileMenu.add(about);
		
		
		JMenuItem quit = new JMenuItem("Quit");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { 
				System.exit(0);
			}
		});
		fileMenu.add(quit);
		
		
		/////////////////////////////////////////////////////////////
		//
		//		Navigation Menu
		//
		/////////////////////////////////////////////////////////////
		JMenuItem reset = new JMenuItem("Reset (ESC)");
		reset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { 
			
			}
		});
		navMenu.add(reset);
		
		
		JMenuItem increase = new JMenuItem("Increase Iterations (+ or l)");
		increase.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { 
				double minx = fractal.getCurrentMinX();
				double maxx = fractal.getCurrentMaxX();
				double miny = fractal.getCurrentMinY();
				double maxy = fractal.getCurrentMaxY();
				fractal.setMaxIterations(fractal.getMaxIterations() + 50);
				fractal.generateFractal(minx, maxx, miny, maxy);
				repaint();
			}
		});
		navMenu.add(increase);
		
		
		JMenuItem decrease = new JMenuItem("Decrese Iterations (- or k)");
		decrease.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { 
				double minx = fractal.getCurrentMinX();
				double maxx = fractal.getCurrentMaxX();
				double miny = fractal.getCurrentMinY();
				double maxy = fractal.getCurrentMaxY();
				fractal.setMaxIterations(fractal.getMaxIterations() - 50);
				fractal.generateFractal(minx, maxx, miny, maxy);
				repaint();
			}
		});
		navMenu.add(decrease);
		
		
		/////////////////////////////////////////////////////////////
		//
		//		Fractal Menu
		//
		/////////////////////////////////////////////////////////////
		for (int i = 0; i < FRACTALS.length; i++) {
			JMenuItem fractalItem = new JMenuItem();
			
			ClassLoader cl = getClass().getClassLoader();
			Class<?> c = null;
			try {
				c = cl.loadClass(FRACTALS[i]);
			} catch (ClassNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			Constructor<?> ctor[] = c.getConstructors();
	
			
			Object[] ctorArgs = new Object[1];
			ctorArgs[0] = (Object) this.panel;

			
			try {
				
				Object y = ctor[0].newInstance(ctorArgs);
	
				final Fractal newFractal = (Fractal) y;
				
				fractalItem.setText(newFractal.getName());
				
				fractalItem.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						
							
							fractal = newFractal;
							fractal.generateFractal(DEFAULT_MINX, DEFAULT_MAXX, DEFAULT_MINY, DEFAULT_MAXY);
							panel.repaint();
							
							
			
						
					}
					
				});
				
				
			
			}  catch (InstantiationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				
				String msg = "Could not invoke: " + c.getName() + " (";
				for (int j = 0; j < ctorArgs.length; j++) {
					msg += ctorArgs[j];
					if (j < ctorArgs.length - 1) {
						msg += ", ";
					}
				}
				msg += ")";
				
				System.out.println(msg);
				
				e1.printStackTrace();
			}
	
			formulaMenu.add(fractalItem);
			
			
		}
		
		
		
		/////////////////////////////////////////////////////////////
		//
		//		Color Menu
		//
		/////////////////////////////////////////////////////////////
		for (int i = 0; i < COLORS.length; i++) {
			JMenuItem colorItem = new JMenuItem();
			
			try {

				ClassLoader cl = getClass().getClassLoader();
				final Class<?> c = cl.loadClass(COLORS[i]);

				Object y = c.newInstance();
				final FractalColorAlgorithm f = (FractalColorAlgorithm) y;
				
				colorItem.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {

						fractal.setColorAlgorithm(f);
						
						double minx = fractal.getCurrentMinX();
						double maxx = fractal.getCurrentMaxX();
						double miny = fractal.getCurrentMinY();
						double maxy = fractal.getCurrentMaxY();
						fractal.generateFractal(minx, maxx, miny, maxy);
						panel.repaint();
					
					}
				});
				
				colorItem.setText(f.getName());
				colorMenu.add(colorItem);
				
			} catch (ClassNotFoundException |  IllegalAccessException |  InstantiationException e1) {
			
				e1.printStackTrace();
			}
		}
		
		JMenuItem internalItem = new JMenuItem("Interior color");
		internalItem.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				colorChoose = new JColorChooser();
				colorDialog = colorChoose.createDialog(com.lowware.fractal.FractalJFrame.this, "Select Interior Color", true, colorChoose, new ColorOKListener(), null);
				colorDialog.setVisible(true);
			}
			
		});
		
		colorMenu.add(internalItem);
		
				
		/////////////////////////////////////////////////////////////
		//
		//		Textbrot Menu
		//
		/////////////////////////////////////////////////////////////
		JMenuItem speakItem = new JMenuItem("Fractal to words");
		JDialog d = new JDialog(this, "Test");
		speakItem.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(com.lowware.fractal.FractalJFrame.this, "Test");
				
			}
			
		});
		textMenu.add(speakItem);
		
		
		
		menuBar.add(fileMenu);
		menuBar.add(navMenu);
		menuBar.add(formulaMenu);
		menuBar.add(colorMenu);
		menuBar.add(textMenu);
		
		return menuBar;
		
		
	}

	

class ColorOKListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Color c = colorChoose.getColor();
		fractal.setInternalRed(c.getRed());
		fractal.setInternalGreen(c.getGreen());
		fractal.setInternalBlue(c.getBlue());
		
		System.out.println("r: " + fractal.getInternalRed() + ", g: " + fractal.getInternalGreen() + ", b: " + fractal.getInternalBlue());
		
		double minx = fractal.getCurrentMinX();
		double maxx = fractal.getCurrentMaxX();
		double miny = fractal.getCurrentMinY();
		double maxy = fractal.getCurrentMaxY();
		fractal.generateFractal(minx, maxx, miny, maxy);
		panel.repaint();
		
	}
}

	
}


